#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <csignal>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "toml.hpp"
#include <list>
#include <typeinfo>
#include <map>

int main()
{
    //获取toml然后转string
    std::ifstream InputStream;
    InputStream.open("./global.toml");
    std::stringstream Strstream;
    Strstream << InputStream.rdbuf();
    InputStream.close();
    std::string a = Strstream.str();

    //string转toml
    std::istringstream re(a);
    auto data = toml::parse<toml::preserve_comments>(re);
    //toml::basic_value<::toml::preserve_comments, std::unordered_map, std::vector> data = toml::parse<toml::preserve_comments>(re);
    std::cout << "data type: " << typeid(data).name() << std::endl;
    //前端显示
    std::cout << "section1-conf1: " << toml::find<std::string>(toml::find(data, "section1"), "conf1") << std::endl;
    //显示备注信息
    int commentsSize = data["section1"].at("conf1").comments().size();

    for( int i = 0; i < commentsSize; i++)
    {
        std::string comment = data["section1"].at("conf1").comments().at(i);
        std::cout << "section1-conf1 comment" << i << ":" << comment << std::endl;
    }

    std::cout << "section1-conf2: " << toml::find<float>(toml::find(data, "section1"), "conf2") << std::endl;
    std::cout << "section1-conf3: " << toml::find<int>(toml::find(data, "section1"), "conf3") << std::endl;

    //获取数组
    std::vector<int> conf6 = toml::find<std::vector<int>>(toml::find(data, "section1"), "conf6");
    std::cout << "section1-conf6 size: " << conf6.size() << ", and value:";
    for(auto iter : conf6)
    {
        std::cout << iter << " ";
    }
    std::cout << std::endl;

    std::vector<double> conf7 = toml::find<std::vector<double>>(toml::find(data, "section1"), "conf7");
    std::cout << "section1-conf7 size: " << conf7.size() << ", and value:";
    for(auto iter : conf7)
    {
        std::cout << iter << " ";
    }
    std::cout << std::endl;

    //获取嵌套数组
    std::vector<std::vector<double>> conf8 = toml::find<std::vector<std::vector<double>>>(toml::find(data, "section1"), "conf8");
    std::cout << "section1-conf8 num: " << conf8.size() * conf8[0].size() << ", and value:" << std::endl;
    for(auto iter : conf8)
    {
        for(auto ite : iter)
        {
            std::cout << ite << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    //前端修改toml
    data["section1"].at("conf1") = "conf11 update";
    data["section1"].at("conf2") = 0.2;
    data["section1"].at("conf3") = 10010;
    //修改数组
    std::vector<int> conf16update {1,2,3,4,5};
    data["section1"].at("conf6") = conf16update;
    //修改嵌套数组
    std::vector<std::vector<float>> conf18update {{1.1,1.2,1.3},
                                                    {2.1,2.2,2.3},
                                                    {3.1,3.2,3.3}};
    data["section1"].at("conf8") = conf18update;
    std::cout << "section1-conf1 update: " << toml::find<std::string>(toml::find(data, "section1"), "conf1") << std::endl;
    std::cout << "section1-conf2 update: " << toml::find<float>(toml::find(data, "section1"), "conf2") << std::endl;
    std::cout << "section1-conf3 update: " << toml::find<int>(toml::find(data, "section1"), "conf3") << std::endl;

    //前端转换为string
    std::string b = toml::concat_to_string(data);

    //保存
    std::ofstream OsWrite2("./globalnew.toml");
    // OsWrite2 << std::setw(80) << b;
    OsWrite2 << b;
    OsWrite2 << std::endl;
    OsWrite2.close();
}