#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>

int SystemRtn(const char* Cmd, char* pbyRtnStr)
{
	FILE *fp;
	int rc = 0;
	char result_buf[256];
	int nOnceLen = 0;
	int nTotalLen = 0;
	int i;

	fp = popen(Cmd, "r");
	if(NULL == fp)
	{
		perror("popen() error!");
		return -1;
	}

	while(fgets(result_buf, 256, fp) != NULL)
	{
		// for (i = 0; i < strlen(result_buf); i++)
		// {
		// 	/*把命令返回的换行符去掉*/
		// 	if('\n' == result_buf[i])
		// 	{
		// 		result_buf[i] = '\0';
		// 	}
		// }

		nOnceLen = strlen(result_buf);
		memcpy(pbyRtnStr+nTotalLen, result_buf, nOnceLen);
		nTotalLen += nOnceLen;
		// printf("cmd[%s] out[%s]\r\n", Cmd, result_buf);
		memset(result_buf, 0, nOnceLen);
	}

	*(pbyRtnStr+nTotalLen) = '\0';

	/*等待命令执行完毕并关闭管道及文件指针*/
	rc = pclose(fp);
	if(-1 == rc)
	{
		perror("cloud fp error!");
		return -1;
	}
	else
	{
		// printf("cmd[%s]child process end status[%d]return[%d]\r\n", Cmd, rc, WEXITSTATUS(rc));
		return 0;
	}
}

int main()
{
    char result[120400] = {0};
    std::string cmd("tar -zcvf test.tgz test.c");
    SystemRtn(cmd.c_str(), result);
    // printf("cmd return:\r\n%s\r\n", result);
    std::string cmdResult(result);
    std::cout << "cmdresult" << cmdResult << std::endl;
}
